--Deleting the reviews table
DROP TABLE IF EXISTS reviews;

--Creating the new reviews table
CREATE TABLE reviews (
	id INT NOT NULL AUTO_INCREMENT,
	PRIMARY KEY (id),
	review VARCHAR(500) NOT NULL,
	datetime_created DATETIME NOT NULL,
	rating INT
);

--Inserting 5 review records
INSERT INTO reviews (id, review, datetime_created, rating) VALUES
	(1, "OMG.... :)", NOW(), 5),
	(2, "Its meeh...", NOW(), 3),
	(3, "The album sucks", NOW(), 2),
	(4, "Solid. Rock and Roll!", NOW(), 5),
	(5, "This album was disappointing.", NOW(), 1);

--Displaying all review records
SELECT * FROM reviews;	

--Displaying all review records with the rating of 5
SELECT * FROM reviews WHERE rating = 5;

--Displaying all review records with the rating of 1
SELECT * FROM reviews WHERE rating = 1;

--Updating all review ratings to 5
UPDATE reviews SET rating = 5;